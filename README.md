# grabmearelease

[![Build Status](https://drone.dotya.ml/api/badges/wanderer/grabmearelease/status.svg)](https://drone.dotya.ml/wanderer/grabmearelease)

this repo holds *sawce* for the one and only release grabber android app written in java - grabmearelease

### Purpose

I want to aggregate release information on my favourite projects, this app is the place for it

currently supported software forge api models:
* Gitea api v1 ---> https://docs.gitea.io/en-us/api-usage/<br />
  reference instance api available at [git.dotya.ml/api/swagger](https://git.dotya.ml/api/swagger)
* GitHub api v3 ---> https://docs.github.com/en/free-pro-team@latest/rest

> **Note:** this app is currently being built 🚧
