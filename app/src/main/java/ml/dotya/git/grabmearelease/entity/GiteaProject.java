package ml.dotya.git.grabmearelease.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import java.util.List;

@Entity(indices = {@Index(value = {"affiliation"},
        unique = true)})
public class GiteaProject {

    @Ignore
    private List<GiteaRelease> lgr;

    public GiteaProject(){}
    public GiteaProject(List<GiteaRelease> lgr){
        this.lgr = lgr;
    }
    public List<GiteaRelease> getProject(){
        return lgr;
    }

    @PrimaryKey
    public int id;

    /* instance + owner + repo */
    @ColumnInfo(name = "affiliation")
    private String affiliation;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAffiliation() {
        return affiliation;
    }

    public void setAffiliation(String affiliation) {
        this.affiliation = affiliation;
    }
}
