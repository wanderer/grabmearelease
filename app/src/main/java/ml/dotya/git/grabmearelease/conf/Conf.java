package ml.dotya.git.grabmearelease.conf;

public class Conf {
    public final static String
            REPO_URL = "https://git.dotya.ml/wanderer/grabmearelease",
            AUTHOR_URL = "https://git.dotya.ml/wanderer",
            GITEA_API_REPOS_URI = "/api/v1/repos",
            PROTO = "https://";
}
