package ml.dotya.git.grabmearelease.model;

import java.util.List;

public class GiteaProject {
    private List<GiteaRelease> project;
    private String affiliation;

    /* No args constructor for serialization */
    public GiteaProject(){}

    public GiteaProject(List<GiteaRelease> project){
        this.project = project;
    }

    public List<GiteaRelease> get_project(){
        return project;
    }
    public void set_project(List<GiteaRelease> project){
        this.project = project;
    }
}
