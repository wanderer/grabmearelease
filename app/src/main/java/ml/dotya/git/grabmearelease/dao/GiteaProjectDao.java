package ml.dotya.git.grabmearelease.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import ml.dotya.git.grabmearelease.entity.GiteaProject;
import ml.dotya.git.grabmearelease.entity.GiteaRelease;

@Dao
public interface GiteaProjectDao {
    @Query("SELECT * FROM GiteaRelease")
    List<GiteaRelease> gimme_all();

    @Insert
    void insert_all(GiteaProject... GiteaProject);

    @Delete
    void delete(GiteaProject GiteaProject);
}
