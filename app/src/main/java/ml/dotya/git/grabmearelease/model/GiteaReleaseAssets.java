package ml.dotya.git.grabmearelease.model;

import java.util.Date;

public class GiteaReleaseAssets {
    private String uuid;
    private int size;
    private String name;
    private int id;
    private int download_count;
    private Date created_at;
    private String browser_download_url;

    /* No args constructor for serialization */
    public GiteaReleaseAssets() {}

    public GiteaReleaseAssets(String uuid, int size, String name, int id, int download_count, Date created_at, String browser_download_url) {
        this.uuid = uuid;
        this.size = size;
        this.name = name;
        this.id = id;
        this.download_count = download_count;
        this.created_at = created_at;
        this.browser_download_url = browser_download_url;
    }

    public void set_uuid(String uuid) {
        this.uuid = uuid;
    }
    public void set_size(int size) {
        this.size = size;
    }
    public void set_name() {
        this.name = name;
    }
    public void set_id(int id) {
        this.id = id;
    }
    public void set_download_count(int download_count) {
        this.download_count = download_count;
    }
    public void set_created_at(Date created_at) {
        this.created_at = created_at;
    }
    public void set_browser_download_url(String browser_download_url) {
        this.browser_download_url = browser_download_url;
    }


    public String gimme_uuid() {
        return uuid;
    }
    public int gimme_size() {
        return size;
    }
    public String gimme_name() {
        return name;
    }
    public int gimme_id() {
        return id;
    }
    public int gimme_download_count() {
        return download_count;
    }
    public Date gimme_created_at() {
        return created_at;
    }
    public String gimme_browser_download_url() {
        return browser_download_url;
    }
}
