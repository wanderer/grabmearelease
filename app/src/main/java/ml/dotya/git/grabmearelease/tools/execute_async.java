package ml.dotya.git.grabmearelease.tools;

import java.util.concurrent.Executor;

public class execute_async implements Executor {
    public void execute(Runnable r) {
        new Thread(r).start();
    }
}
