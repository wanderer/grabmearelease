package ml.dotya.git.grabmearelease.ui.releases;

import ml.dotya.git.grabmearelease.R;
import ml.dotya.git.grabmearelease.adapter.GiteaReleaseAdapter;
import ml.dotya.git.grabmearelease.db.Db;
import ml.dotya.git.grabmearelease.entity.GiteaProject;
import ml.dotya.git.grabmearelease.entity.GiteaRelease;
import ml.dotya.git.grabmearelease.tools.execute_async;

import android.view.MenuItem;
import android.view.View;
import androidx.appcompat.widget.PopupMenu;
import androidx.room.Room;

import com.google.android.material.snackbar.Snackbar;

import java.util.List;
import java.util.concurrent.Executor;

public class ReleaseItemMenuItemClickListener implements PopupMenu.OnMenuItemClickListener {

    private final View view;
    private int position;
    private String name;
    private List<GiteaRelease> lgr;
    GiteaReleaseAdapter adapter;

    public ReleaseItemMenuItemClickListener(View view, int position, String name, List<GiteaRelease> lgr) {
        this.view = view;
        this.position = position;
        this.name = name;
        this.lgr = lgr;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        adapter = new GiteaReleaseAdapter(lgr);
        int id = item.getItemId();
        if (id == R.id.release_action_refresh){
            Snackbar.make(view,"refresh pls", Snackbar.LENGTH_LONG).show();
            return true;
        }
        else if (id == R.id.release_action_visit){
            Snackbar.make(view,"toaaaaaast", Snackbar.LENGTH_LONG).show();
            return true;
        }
        else if (id == R.id.release_action_edit){
            Snackbar.make(view,"toast edit", Snackbar.LENGTH_SHORT).show();
            return true;
        }
        else if (id == R.id.release_action_delete){
            rm_item();
            return true;
        }
        return false;
    }

    private void rm_item() {
        Executor executor = new execute_async();
        Db db = Room.databaseBuilder(view.getContext (), Db.class, "grabmeareleasedb").build();
        executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    db.gr_Dao().delete(lgr.get(0));
                    lgr.remove(position);
                    adapter.notifyItemRemoved(position);
                    adapter.notifyItemRangeChanged(position, lgr.size());
                    Snackbar.make(view,"deleted " + name, Snackbar.LENGTH_SHORT).show();
                } catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }
}
