package ml.dotya.git.grabmearelease.adapter;

import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import ml.dotya.git.grabmearelease.R;
import ml.dotya.git.grabmearelease.db.Db;
import ml.dotya.git.grabmearelease.entity.GiteaRelease;
import ml.dotya.git.grabmearelease.ui.releases.ReleaseItemMenuItemClickListener;

public class GiteaReleaseAdapter extends RecyclerView.Adapter<GiteaReleaseAdapter.ViewHolder> {
    private final List<GiteaRelease> release_list;
    Db db;

    public GiteaReleaseAdapter(List<GiteaRelease> release_list) {
        this.release_list = release_list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        db = Room.databaseBuilder(parent.getContext (), Db.class, "grabmeareleasedb").build();

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View release_item = layoutInflater.inflate(R.layout.release_item, parent, false);
        return new ViewHolder(release_item);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final GiteaRelease release = release_list.get(position);
        holder.release_name.setText(release_list.get(position).getName());
        String body = release_list.get(position).getBody();
        if (!body.equals("")) {
            holder.release_description.setText(body);
            holder.relativeLayout.findViewById(R.id.text_release_description).setVisibility(View.VISIBLE);
        } else {
            holder.release_description.setText("");
            holder.relativeLayout.findViewById(R.id.text_release_description).setVisibility(View.GONE);
        }
        holder.release_url.setText(release_list.get(position).getHtml_url());
//        holder.author.setText(release_list.get(position).getAuthor().gimme_username());
        /* todo - implement author dao */
        holder.author.setText("wanderer");
        holder.release_date.setText(release_list.get(position).getPublished_at());

        holder.relativeLayout.findViewById(R.id.release_item_vert_menu).setOnClickListener(view -> showPopupMenu(holder.relativeLayout.findViewById(R.id.release_item_vert_menu), position, release.getName(), release_list));

        holder.relativeLayout.findViewById(R.id.text_release_url).setOnClickListener(v -> {
           String link = release_list.get(position).getHtml_url();
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(link));
            v.getContext().startActivity(intent);
        });

        holder.relativeLayout.findViewById(R.id.menupls).setOnClickListener(view -> Snackbar.make(view, "info button on release card "+ release.getName(), Snackbar.LENGTH_LONG).show());
    }

    @Override
    public int getItemCount() {
        if (release_list == null){
            return 0;
        }
        return release_list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView release_name;
        public TextView release_description;
        public TextView release_url;
        public TextView author;
        public TextView release_date;
        public RelativeLayout relativeLayout;
        public ViewHolder(View itemView) {
            super(itemView);
            this.release_name = (TextView) itemView.findViewById(R.id.release_name);
            this.release_description = (TextView) itemView.findViewById(R.id.text_release_description);
            this.release_url = (TextView) itemView.findViewById(R.id.text_release_url);
            this.author = (TextView) itemView.findViewById(R.id.text_release_author);
            this.release_date = (TextView) itemView.findViewById(R.id.text_release_date);
            itemView.findViewById(R.id.release_name).setSelected(true);
            itemView.findViewById(R.id.text_release_url).setSelected(true);
            itemView.findViewById(R.id.text_release_date).setSelected(true);
            relativeLayout = (RelativeLayout)itemView.findViewById(R.id.relativeLayout);
        }
    }

    private void showPopupMenu(View view, int position, String name, List<GiteaRelease> release_list) {
        PopupMenu popup = new PopupMenu(view.getContext(), view);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.menu_release_more, popup.getMenu());
        popup.setOnMenuItemClickListener(new ReleaseItemMenuItemClickListener(view, position, name, release_list));
        popup.show();
    }
}
