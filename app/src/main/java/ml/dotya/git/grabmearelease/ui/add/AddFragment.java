package ml.dotya.git.grabmearelease.ui.add;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ml.dotya.git.grabmearelease.Main;
import ml.dotya.git.grabmearelease.R;
import ml.dotya.git.grabmearelease.action.add_project;
import ml.dotya.git.grabmearelease.databinding.FragmentAddBinding;

public class AddFragment extends Fragment {

    private AddViewModel addViewModel;
    private FragmentAddBinding binding;
    private ProgressBar bar;

    private String instance_url;
    private String owner;
    private String repo;

    public static String INSTANCE_URL = "p_instance_url";
    public static String OWNER = "p_owner";
    public static String REPO = "p_repo";

    private EditText et_instance_url;
    private EditText et_owner;
    private EditText et_repo;

    public AddFragment() {
    }

    public static AddFragment newInstance(String p_instance_url, String p_owner, String p_repo) {
        AddFragment fragment = new AddFragment();
        Bundle args = new Bundle();
        args.putString(AddFragment.INSTANCE_URL, p_instance_url);
        args.putString(AddFragment.OWNER, p_owner);
        args.putString(AddFragment.REPO, p_repo);
        fragment.setArguments(args);
        return fragment;
    }

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        addViewModel = new ViewModelProvider(this).get(AddViewModel.class);

        binding = FragmentAddBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        Main.hide_fab();
        if (instance_url != null){
            et_instance_url = root.findViewById(R.id.text_add_project_instance_url);
            et_instance_url.setText(instance_url);
        }
        if (owner != null){
            et_owner = root.findViewById(R.id.text_add_project_owner);
            et_owner.setText(owner);
        }
        if (repo != null){
            et_repo = root.findViewById(R.id.text_add_project_repo);
            et_repo.setText(repo);
        }

        bar = (ProgressBar) root.findViewById(R.id.progressBar);
        bar.setVisibility(View.GONE);

        binding.buttonAddNewRelease.setOnClickListener(view -> get_project_info(root));

        return root;
    }

    private void get_project_info(View view){
        et_instance_url = view.findViewById(R.id.text_add_project_instance_url);
        et_owner = view.findViewById(R.id.text_add_project_owner);
        et_repo = view.findViewById(R.id.text_add_project_repo);
        set_instance_url(et_instance_url.getText().toString());
        set_owner(et_owner.getText().toString());
        set_repo(et_repo.getText().toString());
        if (instance_url != null && owner != null && repo != null){
            bar.setVisibility(View.VISIBLE);
            add_project project = new add_project(instance_url, owner, repo);
            project.add(view);
            bar.setVisibility(View.GONE);
        }
    }

    Pattern whitespace_pattern = Pattern.compile("\\s");
    private void set_instance_url(String instance_url){
        Matcher m = whitespace_pattern.matcher(instance_url);
        if (instance_url.length() != 0 && !m.find()){
            this.instance_url = instance_url;
        }
    }
    private void set_owner(String owner){
        Matcher m = whitespace_pattern.matcher(owner);
        if (owner.length() != 0 && !m.find()){
            this.owner = owner;
        }
    }
    private void set_repo(String repo){
        Matcher m = whitespace_pattern.matcher(repo);
        if (repo.length() != 0 && !m.find()){
            this.repo = repo;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            instance_url = getArguments().getString(INSTANCE_URL);
            owner = getArguments().getString(OWNER);
            repo = getArguments().getString(REPO);
        }
    }
}