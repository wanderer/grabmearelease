package ml.dotya.git.grabmearelease.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import ml.dotya.git.grabmearelease.entity.GiteaRelease;

@Dao
public interface GiteaReleaseDao {
    @Query("SELECT * FROM GiteaRelease")
    List<GiteaRelease> gimme_all();

    @Insert
    void insert_all(GiteaRelease... GiteaRelease);

    @Delete
    void delete(GiteaRelease GiteaRelease);
}
