package ml.dotya.git.grabmearelease.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity(foreignKeys = @ForeignKey(entity = GiteaProject.class,
        parentColumns = "affiliation",
        childColumns = "affiliate_project",
        onDelete = ForeignKey.CASCADE), indices = {@Index(value = {"affiliate_project"},
        unique = true)})
public class GiteaRelease {
    @PrimaryKey
    public int id;

    @ColumnInfo(name = "tag_name")
    private String tag_name;

    @ColumnInfo(name = "target_commitish")
    private String target_commitish;

    @ColumnInfo(name = "name")
    private String name;

    @ColumnInfo(name = "body")
    private String body;

    @ColumnInfo(name = "url")
    private String url;

    @ColumnInfo(name = "html_url")
    private String html_url;

    @ColumnInfo(name = "tarball_url")
    private String tarball_url;

    @ColumnInfo(name = "zipball_url")
    private String zipball_url;

    @ColumnInfo(name = "draft")
    private boolean draft;

    @ColumnInfo(name = "prerelease")
    private boolean prerelease;

    @ColumnInfo(name = "created_at")
    private String created_at;

    @ColumnInfo(name = "published_at")
    private String published_at;

    @ColumnInfo(name = "affiliate_project")
    private String affiliate_project;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTag_name() {
        return tag_name;
    }

    public void setTag_name(String tag_name) {
        this.tag_name = tag_name;
    }

    public String getTarget_commitish() {
        return target_commitish;
    }

    public void setTarget_commitish(String target_commitish) {
        this.target_commitish = target_commitish;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getHtml_url() {
        return html_url;
    }

    public void setHtml_url(String html_url) {
        this.html_url = html_url;
    }

    public String getTarball_url() {
        return tarball_url;
    }

    public void setTarball_url(String tarball_url) {
        this.tarball_url = tarball_url;
    }

    public String getZipball_url() {
        return zipball_url;
    }

    public void setZipball_url(String zipball_url) {
        this.zipball_url = zipball_url;
    }

    public boolean isDraft() {
        return draft;
    }

    public void setDraft(boolean draft) {
        this.draft = draft;
    }

    public boolean isPrerelease() {
        return prerelease;
    }

    public void setPrerelease(boolean prerelease) {
        this.prerelease = prerelease;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getPublished_at() {
        return published_at;
    }

    public void setPublished_at(String published_at) {
        this.published_at = published_at;
    }

    public String getAffiliate_project() {
        return affiliate_project;
    }

    public void setAffiliate_project(String affiliate_project) {
        this.affiliate_project = affiliate_project;
    }
}
