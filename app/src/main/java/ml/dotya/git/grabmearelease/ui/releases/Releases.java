package ml.dotya.git.grabmearelease.ui.releases;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import ml.dotya.git.grabmearelease.Main;
import ml.dotya.git.grabmearelease.R;
import ml.dotya.git.grabmearelease.adapter.GiteaReleaseAdapter;
import ml.dotya.git.grabmearelease.conf.Conf;
import ml.dotya.git.grabmearelease.databinding.FragmentReleasesBinding;
import ml.dotya.git.grabmearelease.db.Db;
import ml.dotya.git.grabmearelease.entity.GiteaProject;
import ml.dotya.git.grabmearelease.entity.GiteaRelease;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import ml.dotya.git.grabmearelease.tools.execute_async;
import okhttp3.Headers;
import com.codepath.asynchttpclient.AsyncHttpClient;
import com.codepath.asynchttpclient.callback.TextHttpResponseHandler;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;

import static androidx.core.content.PermissionChecker.checkSelfPermission;

public class Releases extends Fragment {

    private ReleasesViewModel releasesViewModel;
    private FragmentReleasesBinding binding;

    private static final int PERMISSION_REQUEST_CODE = 1;
    private final Calendar calendar = Calendar.getInstance();
    Db db;

    private GiteaReleaseAdapter adapter;
    private List<GiteaRelease> lgr;
    private AsyncHttpClient client;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        releasesViewModel = new ViewModelProvider(this).get(ReleasesViewModel.class);

        binding = FragmentReleasesBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        Main.show_fab();

        Date date = calendar.getTime();
        db = Room.databaseBuilder(root.getContext (), Db.class, "grabmeareleasedb").build();

        Executor executor = new execute_async();
        CountDownLatch countDownLatch = new CountDownLatch(1);
        executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    lgr = db.gr_Dao().gimme_all();
                    countDownLatch.countDown();
                } catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

        try {
            countDownLatch.await();
        } catch (InterruptedException e){
            e.printStackTrace();
        }

        RecyclerView recyclerView = root.findViewById(R.id.recycler_releases);
        adapter = new GiteaReleaseAdapter(lgr);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(root.getContext()));
        recyclerView.setAdapter(adapter);


        root.findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                client = new AsyncHttpClient();
                int result = checkSelfPermission(root.getContext(), Manifest.permission.INTERNET);
                if (result == PackageManager.PERMISSION_GRANTED){
                    Snackbar.make(view,"permission granted", Snackbar.LENGTH_SHORT).show();
                    test_get_pls(client);
                } else {
                    Snackbar.make(view,"we don't have the permission", Snackbar.LENGTH_SHORT).show();
                    requestPermission();
                }
            }
        });
        return root;
    }


    public void add_project(String instance_url, String owner, String repo){};

    private void test_get_pls(AsyncHttpClient client){
        String proto = "https://";
        String instance_url = "git.dotya.ml";
        String owner = "wanderer";
        String repo = "docker-fedora-hugo";
        String uri = proto + instance_url + Conf.GITEA_API_REPOS_URI + "/" + owner + "/" + repo + "/releases";

        client.get(uri, new TextHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Headers headers, String response) {
                Log.d("DEBUG", response);
                Snackbar.make(binding.getRoot(), response, Snackbar.LENGTH_LONG).show();

                final Gson gson = new Gson();
                Type collection_type = new TypeToken<List<GiteaRelease>>(){}.getType();
                List<GiteaRelease> gr = gson.fromJson(response, collection_type);
                Log.d("DEBUG", "response successfully deserialized.");
                GiteaProject gp = new GiteaProject(gr);
                lgr.addAll(gp.getProject());
                adapter.notifyDataSetChanged();

            }
            @Override
            public void onFailure(int statusCode, @Nullable Headers headers, String errorResponse, @Nullable Throwable throwable) {
                Log.d("DEBUG", errorResponse);
            }
        });
    }


    private void requestPermission(){
        if(shouldShowRequestPermissionRationale(Manifest.permission.INTERNET)){
            Snackbar.make(binding.getRoot(), "we need the INTERNET permission to get latest releases", Snackbar.LENGTH_LONG).show();
        }
        else {
            requestPermissions(new String[]{Manifest.permission.INTERNET},PERMISSION_REQUEST_CODE);
        }
    }

@Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}