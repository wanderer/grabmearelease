package ml.dotya.git.grabmearelease.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import ml.dotya.git.grabmearelease.dao.GiteaProjectDao;
import ml.dotya.git.grabmearelease.dao.GiteaReleaseDao;
import ml.dotya.git.grabmearelease.entity.GiteaProject;
import ml.dotya.git.grabmearelease.entity.GiteaRelease;

@Database(entities = {GiteaRelease.class, GiteaProject.class}, version = 1, exportSchema = false)
public abstract class Db extends RoomDatabase {
    public abstract GiteaReleaseDao gr_Dao();
    public abstract GiteaProjectDao gp_Dao();
}
