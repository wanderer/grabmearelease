package ml.dotya.git.grabmearelease.model;

import java.util.Date;

public class GiteaReleaseAuthor {
    private int id;
    private String login;
    private String full_name;
    private String email;
    private String avatar_url;
    private String language;
    private String username;
    private Date created;

    /* No args constructor for serialization */
    public GiteaReleaseAuthor(){}

    public GiteaReleaseAuthor(int id, String login, String full_name, String email, String avatar_url, String language, String username, Date created){
        this.id = id;
        this.login = login;
        this.full_name = full_name;
        this.email = email;
        this.avatar_url = avatar_url;
        this.language = language;
        this.username = username;
        this.created = created;
    }

    public void set_id(int id) {
        this.id = id;
    }
    public void set_login(String login) {
        this.login = login;
    }
    public void set_full_name(String full_name) {
        this.full_name = full_name;
    }
    public void set_email(String email) {
        this.email = email;
    }
    public void set_avatar_url(String avatar_url) {
        this.avatar_url = avatar_url;
    }
    public void set_language(String language) {
        this.language = language;
    }
    public void set_username(String username) {
        this.username = username;
    }
    public void set_created(Date created) {
        this.created = created;
    }


    public int gimme_id() {
        return id;
    }
    public String gimme_login() {
        return login;
    }
    public String gimme_full_name() {
        return full_name;
    }
    public String gimme_email() {
        return email;
    }
    public String gimme_avatar_url() {
        return avatar_url;
    }
    public String gimme_language() {
        return language;
    }
    public String gimme_username() {
        return username;
    }
    public Date gimme_created() {
        return created;
    }

}
