package ml.dotya.git.grabmearelease.model;

import java.util.Date;
import java.util.List;

public class GiteaRelease {
    private int id;
    private String tag_name;
    private String target_commitish;
    private String name;
    private String body;
    private String url;
    private String html_url;
    private String tarball_url;
    private String zipball_url;
    private boolean draft;
    private boolean prerelease;
    private Date created_at;
    private Date published_at;

    private GiteaReleaseAuthor author;

    private List<GiteaReleaseAssets> asset_list;


    /* No args constructor for serialization */
    public GiteaRelease(){}

    public GiteaRelease(String name, String body, String html_url, boolean draft, boolean prerelease, Date published_at, String tag_name, String target_commitish, GiteaReleaseAuthor author){
        this.name = name;
        this.body = body;
        this.html_url = html_url;
        this.draft = draft;
        this.prerelease = prerelease;
        this.published_at = published_at;
        this.tag_name = tag_name;
        this.target_commitish = target_commitish;
        this.author = author;
    }


    public void set_id(int id) {
        this.id = id;
    }
    public void set_tag_name(String tag_name) {
        this.tag_name= tag_name;
    }
    public void set_target_commitish(String target_commitish) {
        this.target_commitish = target_commitish;
    }
    public void set_name(String name) {
        this.name = name;
    }
    public void set_body(String body) {
        this.body = body;
    }
    public void set_url(String url) {
        this.url = url;
    }
    public void set_html_url(String html_url) {
        this.html_url = html_url;
    }
    public void set_tarball_url(String tarball_url) {
        this.tarball_url = tarball_url;
    }
    public void set_zipball_url(String zipball_url) {
        this.zipball_url = zipball_url;
    }
    public void set_is_draft(boolean draft) {
        this.draft = draft;
    }
    public void set_is_prerelease(boolean prerelease) {
        this.prerelease = prerelease;
    }
    public void set_created_at(Date created_at) {
        this.created_at = created_at;
    }
    public void set_published_at(Date published_at) {
        this.published_at = published_at;
    }
    public void set_author(GiteaReleaseAuthor author) {
        this.author = author;
    }
    public void set_assets(List<GiteaReleaseAssets> asset_list) {
        this.asset_list = asset_list;
    }


    public int getId() {
        return id;
    }
    public String getTag_name() {
        return tag_name;
    }
    public String getTarget_commitish() {
        return target_commitish;
    }
    public String getName() {
        return name;
    }
    public String getBody() {
        return body;
    }
    public String getUrl() {
        return url;
    }
    public String getHtml_url() {
        return html_url;
    }
    public String getTarball_url() {
        return tarball_url;
    }
    public String getZipball_url() {
        return zipball_url;
    }
    public boolean isDraft() {
        return draft;
    }
    public boolean isPrerelease() {
        return prerelease;
    }
    public Date getCreated_at() {
        return created_at;
    }
    public Date getPublished_at() {
        return published_at;
    }
    public GiteaReleaseAuthor getAuthor() {
        return author;
    }
    public List<GiteaReleaseAssets> getAssets() {
        return asset_list;
    }

}
