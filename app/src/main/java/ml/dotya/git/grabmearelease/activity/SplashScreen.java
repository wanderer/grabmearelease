package ml.dotya.git.grabmearelease.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import ml.dotya.git.grabmearelease.Main;

public class SplashScreen extends Activity {
    private static final int SPLASH_TIME_OUT = 400;

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent start_main = new Intent(SplashScreen.this, Main.class);
                startActivity(start_main);
                finish();
            }
        }, SPLASH_TIME_OUT);
    }
}
