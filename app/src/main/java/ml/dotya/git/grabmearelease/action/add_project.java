package ml.dotya.git.grabmearelease.action;

import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.room.Room;

import com.codepath.asynchttpclient.AsyncHttpClient;
import com.codepath.asynchttpclient.callback.TextHttpResponseHandler;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;

import ml.dotya.git.grabmearelease.adapter.GiteaReleaseAdapter;
import ml.dotya.git.grabmearelease.conf.Conf;
import ml.dotya.git.grabmearelease.db.Db;
import ml.dotya.git.grabmearelease.entity.GiteaProject;
import ml.dotya.git.grabmearelease.entity.GiteaRelease;
import ml.dotya.git.grabmearelease.tools.execute_async;
import okhttp3.Headers;

public class add_project {
    private final String instance_url;
    private final String owner;
    private final String repo;
    private String release_url;
    private static final AsyncHttpClient client = new AsyncHttpClient();
    private boolean instance_up;
    private boolean api_accessible;
    private boolean project_accessible;
    private CountDownLatch countDownLatch;
    private final Executor executor = new execute_async();
    private Db db;
    private GiteaReleaseAdapter adapter;
    private List<GiteaRelease> lgr;

    public add_project(String instance_url, String owner, String repo){
        this.instance_url = instance_url;
        this.owner = owner;
        this.repo = repo;
    }

    public void add(View view) {
        executor.execute(() -> {
            try {
                lgr = db.gr_Dao().gimme_all();
            } catch (Exception e){
                e.printStackTrace();
            }
        });
        countDownLatch = new CountDownLatch(3);
        check_instance_up();
        Executor executor = this.executor;
        db = Room.databaseBuilder(view.getContext (), Db.class, "grabmeareleasedb").build();

        check_api_accessible();
        check_project_accessible();
        executor.execute(() -> {
            try {
                boolean do_we_add = checks_check_out(view);
                if (do_we_add){
                    AsyncHttpClient client = new AsyncHttpClient();
                    /* addition action here */
                    String uri =  Conf.PROTO + instance_url + Conf.GITEA_API_REPOS_URI + "/" + owner + "/" + repo + "/releases";

                    client.get(uri, new TextHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Headers headers, String response) {
                            Log.d("DEBUG", response);
                            Snackbar.make(view, response, Snackbar.LENGTH_LONG).show();

                            final Gson gson = new Gson();
                            Type collection_type = new TypeToken<List<GiteaRelease>>(){}.getType();
                            List<GiteaRelease> gr = gson.fromJson(response, collection_type);
                            Log.d("DEBUG", "response successfully deserialized.");
                            GiteaProject gp = new GiteaProject(gr);
                            adapter = new GiteaReleaseAdapter(lgr);

                            executor.execute(() -> {
                                try {
                                    db.gp_Dao().insert_all(gp);
                                    db.gr_Dao().insert_all(gr.toArray(gr.toArray(new GiteaRelease[0])));
                                    lgr.addAll(gp.getProject());
                                    adapter.notifyDataSetChanged();
                                } catch (Exception e){
                                    e.printStackTrace();
                                }
                            });
                        }
                        @Override
                        public void onFailure(int statusCode, @Nullable Headers headers, String errorResponse, @Nullable Throwable throwable) {
                            Log.d("DEBUG", errorResponse);
                        }
                    });
                }
            } catch (InterruptedException e){
                e.printStackTrace();
            }
        });
    }

    private boolean checks_check_out(View view) throws InterruptedException {
        countDownLatch.await();
        if (instance_up && api_accessible && project_accessible){
            String msg = "checks passed, adding project for release tracking.";
            Log.d("DEBUG", msg);
            Snackbar.make(view, msg, Snackbar.LENGTH_LONG).show();
            return true;
        }
        return false;
    }

    private void check_instance_up(){
        HttpURLConnection urlConnection;

        System.setProperty("http.keepAlive", "false");
        try {
            URL url = new URL(Conf.PROTO + instance_url);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("HEAD");
            HttpURLConnection finalUrlConnection = urlConnection;
            executor.execute(() -> {
                boolean exc = false;
                try {
                    finalUrlConnection.getInputStream().close();
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                    exc = true;
                } catch (IOException e) {
                    e.printStackTrace();
                    exc = true;
                } finally {
                    if (finalUrlConnection != null && !exc) {
                        instance_up = true;
                        finalUrlConnection.disconnect();
                        Log.d("DEBUG", "add project: instance " + Conf.PROTO + instance_url + " appears to be online!");
                        countDownLatch.countDown();
                    }
                }
            });
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void check_api_accessible(){
        String url = Conf.PROTO + instance_url + "/api/v1/settings/api";

        client.get(url, new TextHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Headers headers, String response) {
                api_accessible = true;
                Log.d("DEBUG", response);
                Log.d("DEBUG", "add project: api at " + Conf.PROTO + instance_url + " appears to be accessible!");
                countDownLatch.countDown();
            }
            @Override
            public void onFailure(int statusCode, @Nullable Headers headers, String errorResponse, @Nullable Throwable throwable) {
                Log.d("DEBUG", errorResponse);
                countDownLatch.countDown();
            }
        });
    }
    private void check_project_accessible(){
        String url = Conf.PROTO + instance_url + Conf.GITEA_API_REPOS_URI + "/" + owner + "/" + repo;

        client.get(url, new TextHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Headers headers, String response) {
                project_accessible = true;
                Log.d("DEBUG", response);
                Log.d("DEBUG", "add project: project \"" + owner + "/" + repo + "\" at "+ instance_url + " appears to be accessible!");
                countDownLatch.countDown();
            }
            @Override
            public void onFailure(int statusCode, @Nullable Headers headers, String errorResponse, @Nullable Throwable throwable) {
                Log.d("DEBUG", errorResponse);
                countDownLatch.countDown();
            }
        });
    }
}
